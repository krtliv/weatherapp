//
//  AddCityCollectionViewCell.swift
//  weatherapp
//
//  Created by Kyriakos Leivadas on 11/06/2019.
//  Copyright © 2019 Kyriakos Leivadas. All rights reserved.
//

import UIKit
import CoreLocation

protocol CityDelegate {
    func cityAddTapped(name:String) -> ()
    func cityRemovedTapped(index:IndexPath) -> ()
    func cityUpdateTapped(index:IndexPath) ->  ()
}

class AddCityCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var addButton: UIButton!
    var delegate: CityDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 13;
        self.nameTextField.delegate = self;
        // Initialization code
    }
    
    @IBAction func addButtonTapped(_ sender: Any) {
        var userInput = nameTextField.text!
        userInput = userInput.replacingOccurrences(of: " ", with: "+")
        if (self.delegate != nil){
            self.delegate?.cityAddTapped(name: self.nameTextField.text!)
        }
        
    }
}

extension AddCityCollectionViewCell:UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
