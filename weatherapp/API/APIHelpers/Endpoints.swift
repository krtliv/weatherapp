//
//  Endpoints.swift
//  weatherapp
//
//  Created by Kyriakos Leivadas on 11/06/2019.
//  Copyright © 2019 Kyriakos Leivadas. All rights reserved.
//

import Foundation

class Endpoints: NSObject {
    
    static let APIKey = "ecc7f7705bec4c9995e182554191106"
    static let baseAPI = "https://api.worldweatheronline.com/premium/v1/weather.ashx"
    
//    static let forcastUrl = "q=48.85,2.35&mca=no&show_comments=no&num_of_days=5&tp=24&cc=no&format=json" //temporary
//    static let fullUrl = baseAPI + "?key=" + APIKey + "&" + forcastUrl //temporary
    
    
    static func weatherUrl() -> String {
        return baseAPI + "?key=" + APIKey
    }
}
