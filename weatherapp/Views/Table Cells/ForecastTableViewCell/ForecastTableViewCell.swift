//
//  ForecastTableViewCell.swift
//  weatherapp
//
//  Created by Kyriakos Leivadas on 14/06/2019.
//  Copyright © 2019 Kyriakos Leivadas. All rights reserved.
//

import UIKit
import SDWebImage

class ForecastTableViewCell: UITableViewCell {

    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var avgTempLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var weatherDescrLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(weather:WeatherModel) {
        self.minTempLabel.text = "Min: " + weather.mintempC + "c"
        self.maxTempLabel.text = "Max: " + weather.maxtempC + "c"
        self.avgTempLabel.text = "Avg: " + weather.avgtempC + "c"
        self.weatherDescrLabel.text = weather.weatherDesc
        self.weatherImageView.sd_setImage(with: URL(string: weather.weatherIconUrl), placeholderImage: UIImage(named: "placeholder"))
        self.weatherImageView.layer.cornerRadius = 20
        self.dayLabel.text = Utils.getDayOfWeek(weather.date)
    }
    
}
