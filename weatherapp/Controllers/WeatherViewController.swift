//
//  WeatherViewController.swift
//  weatherapp
//
//  Created by Kyriakos Leivadas on 11/06/2019.
//  Copyright © 2019 Kyriakos Leivadas. All rights reserved.
//

import UIKit

let forecastCellIdentifier = "ForecastTableViewCell"

class WeatherViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    var weatherArray:[WeatherModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self;
        tableView.register(UINib.init(nibName: forecastCellIdentifier, bundle: nil), forCellReuseIdentifier: forecastCellIdentifier)
    }

}

extension WeatherViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: forecastCellIdentifier, for: indexPath) as! ForecastTableViewCell
        cell.setupCell(weather: weatherArray[indexPath.row])
        return cell
    }
    
}
