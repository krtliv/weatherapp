//
//  WeatherAPI.swift
//  weatherapp
//
//  Created by Kyriakos Leivadas on 11/06/2019.
//  Copyright © 2019 Kyriakos Leivadas. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class WeatherAPI: NSObject {
    
    static func request(url:String, type:HTTPMethod, params:[String:Any], encoding: ParameterEncoding, headers: HTTPHeaders, completion: @escaping (DataResponse<Any>) -> ()) {
        AF.request(url, method: type, parameters: params, encoding: encoding, headers: headers)
            .responseJSON { response in
                completion(response)
        }
    }
    
    static func getWeatherForcast(params:[String:Any],completion:@escaping ([WeatherModel])->()){
        self.request(url: Endpoints.weatherUrl(),
                     type: .get,
                     params: params,
                     encoding: URLEncoding(destination: .queryString),
                     headers: [:], completion:{response in
                        guard let data = response.data else {return}
                        let json = try? JSON(data: data)
                        if let weatherArray =  json?["data"]["weather"].arrayValue.map({return WeatherModel(json:$0)}){
                            completion(weatherArray)
                        }
                        else {
                            completion([])
                        }
        })
    }
    
}
