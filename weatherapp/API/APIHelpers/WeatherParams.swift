//
//  WeatherParams.swift
//  weatherapp
//
//  Created by Kyriakos Leivadas on 14/06/2019.
//  Copyright © 2019 Kyriakos Leivadas. All rights reserved.
//

import UIKit

class WeatherParams: NSObject {
    
    static func defaultParamsWithCity(name:String) -> [String:Any]{
        return ["q":name,
                "mca":"no",
                "show_comments":"no",
                "num_of_days":"5",
                "tp":"24",
                "cc":"no",
                "format":"json"]
    }

}
