//
//  WeatherCollectionViewCell.swift
//  weatherapp
//
//  Created by Kyriakos Leivadas on 11/06/2019.
//  Copyright © 2019 Kyriakos Leivadas. All rights reserved.
//

import UIKit

class CityCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cityImageView: UIImageView!
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var lastUpdateLabel: UILabel!
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var updateBtn: UIButton!
    var indexPath:IndexPath!
    var delegate:CityDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 13;
        // Initialization code
    }
    
    @IBAction func updateTapped(_ sender: Any) {
        if (self.delegate != nil){
            delegate?.cityUpdateTapped(index: indexPath)
        }
    }
    
    @IBAction func removeTapped(_ sender: Any) {
        if (self.delegate != nil){
            delegate?.cityRemovedTapped(index: indexPath)
        }
    }
    
    func setupCell(city:CityModel) {
        cityName.text = city.name
        lastUpdateLabel.text = "last update \n" + Utils.dateAsString(date: city.lastUpdate, style: .medium)
    }
}
