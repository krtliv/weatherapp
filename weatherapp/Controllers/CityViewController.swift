//
//  ViewController.swift
//  weatherapp
//
//  Created by Kyriakos Leivadas on 11/06/2019.
//  Copyright © 2019 Kyriakos Leivadas. All rights reserved.
//

import UIKit
import MapKit

let addCityIdentifier = "AddCityCollectionViewCell"
let cityIdentifier = "CityCollectionViewCell"

class CityViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    let locationManager = LocationManager.SharedManager
    var vSpinner : UIView?
    var currentCity:String = ""
    var citiesArray:[CityModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set location manager delegate
        locationManager.delegate = self
        
        //register collectionview cells xibs
        collectionView.register(UINib(nibName: cityIdentifier, bundle:nil), forCellWithReuseIdentifier: cityIdentifier)
        collectionView.register(UINib(nibName: addCityIdentifier, bundle:nil), forCellWithReuseIdentifier: addCityIdentifier)
        
        //set collectionView delegate & data source
        collectionView.delegate = self;
        collectionView.dataSource = self;
        
        //load already saved cities from UserDefaults
        //MARK: in a production level an sqlite database should be used.
        citiesArray = Utils.retrieveCities()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is WeatherViewController
        {
            let index = sender as! NSInteger
            let vc = segue.destination as? WeatherViewController
            vc?.weatherArray = citiesArray[index].forcastArray
        }
    }
}

extension CityViewController:LocationUpdateProtocol {
    func locationDidUpdateToLocation(location: CLLocation) {
        locationManager.getPlace(for: location, completion: { placemark in
            self.currentCity = placemark?.locality ?? ""
            self.collectionView.reloadData()
        })
    }
}

//MARK: CityDelegate
extension CityViewController:CityDelegate {
    func cityAddedWith(name: String, forecast: [WeatherModel]) {
        let city = CityModel.init(name: name, forcast: forecast)
        citiesArray.append(city)
        Utils.saveCitiesToUserDefaults(cities: citiesArray)
        collectionView.reloadData()
    }
    
    func cityAddTapped(name: String) {
        //show loader
        showSpinner(onView: self.view)

        WeatherAPI.getWeatherForcast(params: WeatherParams.defaultParamsWithCity(name: name)) { (weatherArray) in
            //hide loader
            self.removeSpinner()
            
            if (weatherArray.count > 0){
                let city = CityModel.init(name: name, forcast: weatherArray)
                self.citiesArray.append(city)
                Utils.saveCitiesToUserDefaults(cities: self.citiesArray)
                self.collectionView.reloadData()
            } else {
                Utils.showAlert(vc: self, message: "\(name) not found, type another city.", title: "Error")
            }
        }
    }
    func cityRemovedTapped(index: IndexPath) {
        citiesArray.remove(at: index.row)
        Utils.saveCitiesToUserDefaults(cities: citiesArray)
        collectionView.reloadData()
    }
    
    func cityUpdateTapped(index: IndexPath) {
        //show loader
        showSpinner(onView: self.view)
        
        WeatherAPI.getWeatherForcast(params: WeatherParams.defaultParamsWithCity(name: citiesArray[index.row].name)) { (weatherArray) in
            
            //hide loader
            self.removeSpinner()
            
            if (weatherArray.count > 0){
                self.citiesArray[index.row].forcastArray = weatherArray
                self.citiesArray[index.row].lastUpdate = Date.init()
                Utils.saveCitiesToUserDefaults(cities: self.citiesArray)
                self.collectionView.reloadData()
            }
            else {
                Utils.showAlert(vc: self, message: "No Forecast found for \(self.citiesArray[index.row].name), try again later.", title: "Error")
            }
        }
    }
}

//MARK: UICollectionViewDelegate
extension CityViewController:UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return citiesArray.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let  addCityCell = collectionView.dequeueReusableCell(withReuseIdentifier: addCityIdentifier, for: indexPath) as! AddCityCollectionViewCell
        let  cityCell = collectionView.dequeueReusableCell(withReuseIdentifier: cityIdentifier, for: indexPath) as! CityCollectionViewCell
        
        if (indexPath.row == citiesArray.count){
            addCityCell.delegate = self;
            addCityCell.nameTextField.text = currentCity as String
            return addCityCell
        } else {
            cityCell.setupCell(city: citiesArray[indexPath.row])
            cityCell.indexPath = indexPath
            cityCell.delegate = self
            return cityCell;
        }
    }
    
}

//MARK: UICollectionViewDataSource
extension CityViewController:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (indexPath.row != citiesArray.count){
            //navigate to weather forecast screen
            if (citiesArray[indexPath.row].forcastArray.count > 0){
                self.performSegue(withIdentifier: "WeatherViewSegue", sender: indexPath.row)
            } else {
                
                //show loader
                showSpinner(onView: self.view)
                
                WeatherAPI.getWeatherForcast(params: WeatherParams.defaultParamsWithCity(name: citiesArray[indexPath.row].name)) { (weatherArray) in
                    
                    //hide loader
                    self.removeSpinner()
                    
                    if (weatherArray.count > 0){
                        self.citiesArray[indexPath.row].forcastArray = weatherArray
                        Utils.saveCitiesToUserDefaults(cities: self.citiesArray)
                        self.performSegue(withIdentifier: "WeatherViewSegue", sender: indexPath.row)
                    }
                    else {
                        Utils.showAlert(vc: self, message: "No Forecast found for \(self.citiesArray[indexPath.row].name), try again later.", title: "Error")
                    }
                }
            }
        }
    }
}

//MARK: UICollectionViewDelegateFlowLayout
extension CityViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.self.width/2 - 20, height: 160)
    }
}


extension CityViewController {
    
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.3)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
}
