//
//  weatherappTests.swift
//  weatherappTests
//
//  Created by Kyriakos Leivadas on 11/06/2019.
//  Copyright © 2019 Kyriakos Leivadas. All rights reserved.
//

import XCTest
import Alamofire
import SwiftyJSON

@testable import weatherapp

class weatherappTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testDayOfDate() {
        let promise = expectation(description: "Converted Day should be Friday")
        if (Utils.getDayOfWeek("2019-06-14") == "Friday"){
            promise.fulfill()
        } else {
            XCTFail("Day is wrong: \(Utils.getDayOfWeek("2019-06-14") ?? "nil"), should be Friday")
        }
        wait(for: [promise], timeout: 1)
    }
    
    func testWeatherForecastAPIStatus200() {
        let promise = expectation(description: "Status code: 200")
        WeatherAPI.request(url: Endpoints.weatherUrl(),
                           type: .get,
                           params: WeatherParams.defaultParamsWithCity(name: "Athens"),
                           encoding: URLEncoding(destination: .queryString),
                           headers: [:], completion:{response in
                            if (response.response!.statusCode == 200){
                                promise.fulfill()
                            } else {
                                XCTFail("Status code: \(response.response!.statusCode)")
                            }
        })
        wait(for: [promise], timeout: 10)
    }
    
    func testWeatherForecastAPIJSONParsing() {
        let promise = expectation(description: "JSON parse success.")
        WeatherAPI.request(url: Endpoints.weatherUrl(),
                           type: .get,
                           params: WeatherParams.defaultParamsWithCity(name: "Athens"),
                           encoding: URLEncoding(destination: .queryString),
                           headers: [:], completion:{response in

                            guard let data = response.data else {return}
                            let json = try? JSON(data: data)
                            if (json?["data"]["weather"].arrayValue.map({return WeatherModel(json:$0)})) != nil{
                                promise.fulfill()
                            }
                            else {
                                XCTFail("JSON error parsing: \(json?.stringValue ?? "nil")")
                            }
                            
                            
        })
        wait(for: [promise], timeout: 10)
    }
    
}
