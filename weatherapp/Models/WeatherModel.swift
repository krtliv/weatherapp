//
//  WeatherModel.swift
//  weatherapp
//
//  Created by Kyriakos Leivadas on 11/06/2019.
//  Copyright © 2019 Kyriakos Leivadas. All rights reserved.
//

import Foundation
import SwiftyJSON

class WeatherModel: NSObject,NSCoding {
    
    var date: String
    var maxtempC: String
    var mintempC: String
    var avgtempC: String
    var weatherDesc: String
    var weatherIconUrl: String

    required init(json: JSON) {
        date = json["date"].stringValue
        maxtempC = json["maxtempC"].stringValue
        mintempC = json["mintempC"].stringValue
        avgtempC = json["avgtempC"].stringValue
        weatherIconUrl = json["hourly"][0]["weatherIconUrl"][0]["value"].stringValue
        weatherDesc = json["hourly"][0]["weatherDesc"][0]["value"].stringValue
    }
    
    required init(coder decoder: NSCoder) {
        self.date = decoder.decodeObject(forKey: "date") as! String
        self.maxtempC = decoder.decodeObject(forKey: "maxtempC") as! String
        self.mintempC = decoder.decodeObject(forKey: "mintempC") as! String
        self.avgtempC = decoder.decodeObject(forKey: "avgtempC") as! String
        self.weatherIconUrl = decoder.decodeObject(forKey: "weatherIconUrl") as! String
        self.weatherDesc = decoder.decodeObject(forKey: "weatherDesc") as! String
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.date, forKey: "date")
        aCoder.encode(self.maxtempC, forKey: "maxtempC")
        aCoder.encode(self.mintempC, forKey: "mintempC")
        aCoder.encode(self.avgtempC, forKey: "avgtempC")
        aCoder.encode(self.weatherIconUrl, forKey: "weatherIconUrl")
        aCoder.encode(self.weatherDesc, forKey: "weatherDesc")
    }
    
}
