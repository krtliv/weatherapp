//
//  LocationManager.swift
//  weatherapp
//
//  Created by Kyriakos Leivadas on 14/06/2019.
//  Copyright © 2019 Kyriakos Leivadas. All rights reserved.
//

import MapKit

protocol LocationUpdateProtocol {
    func locationDidUpdateToLocation(location : CLLocation)
}

class LocationManager: NSObject {
    
    static let SharedManager = LocationManager()
    private let locationManager = CLLocationManager()
    var currentLocation : CLLocation?
    var delegate : LocationUpdateProtocol!
    
    
    public var exposedLocation: CLLocation? {
        return self.locationManager.location
    }
    
    func getPlace(for location: CLLocation,
                  completion: @escaping (CLPlacemark?) -> Void) {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location) { placemarks, error in
            guard error == nil else {
                print("*** Error in \(#function): \(error!.localizedDescription)")
                completion(nil)
                return
            }
            guard let placemark = placemarks?[0] else {
                print("*** Error in \(#function): placemark is nil")
                completion(nil)
                return
            }
            completion(placemark)
        }
    }
    
    override init() {
        super.init()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
}


// MARK: - Core Location Delegate
extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations[0]
        if ((delegate) != nil){
            delegate.locationDidUpdateToLocation(location: locations[0])
            self.locationManager.stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined         : print("notDetermined")        // permission not asked for yet
        case .authorizedWhenInUse   : print("authorizedWhenInUse")  // authorized
        case .authorizedAlways      : print("authorizedAlways")     // authorized
        case .restricted            : print("restricted")           // restricted
        case .denied                : print("denied")               // denied
        @unknown default            : print("uknown")               // unknown state
        }
    }
}
