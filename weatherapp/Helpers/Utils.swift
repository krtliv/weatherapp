//
//  Utils.swift
//  weatherapp
//
//  Created by Kyriakos Leivadas on 14/06/2019.
//  Copyright © 2019 Kyriakos Leivadas. All rights reserved.
//

import UIKit

class Utils: NSObject {
    
    //show an alert dialog
    static func showAlert(vc:UIViewController, message:String, title:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            @unknown default:
                print("unknown")
            }}))
        vc.present(alert, animated: true, completion: nil)
    }
    
    //convert date to day of the week
    static func getDayOfWeek(_ today:String) -> String? {
        let df  = DateFormatter()
        df.dateFormat = "YYYY-MM-dd"
        let date = df.date(from: today)!
        df.dateFormat = "EEEE"
        return df.string(from: date);
    }
    
    //return date as string
    static func dateAsString(date: Date, style: DateFormatter.Style) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = style
        dateFormatter.timeStyle = style
        return dateFormatter.string(from: date)
    }
    
    //save items to userdefaults
    //MARK: in a production level an sqlite database should be used to save the cities.
    static func saveCitiesToUserDefaults(cities:[CityModel]) {
        guard let data = try? NSKeyedArchiver.archivedData(withRootObject: cities, requiringSecureCoding: false) else {return}
        UserDefaults.standard.set(data, forKey: "myCities")
        UserDefaults.standard.synchronize()
    }
    
    //retieve items from userdefaults
    //MARK: in a production level an sqlite database should be used to save the cities.
    static func retrieveCities() -> [CityModel] {
        if let data = UserDefaults.standard.object(forKey: "myCities") as? NSData {
            guard let cities = try? NSKeyedUnarchiver.unarchiveObject(with: data as Data) else {return []}
            return cities as! [CityModel];
        }
        return []
    }
    
}
