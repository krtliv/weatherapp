//
//  CityModel.swift
//  weatherapp
//
//  Created by Kyriakos Leivadas on 11/06/2019.
//  Copyright © 2019 Kyriakos Leivadas. All rights reserved.
//

import UIKit

class CityModel: NSObject, NSCoding {
    var name: String
    var forcastArray: [WeatherModel]
    var lastUpdate: Date
    
    init(name: String, forcast: [WeatherModel]) {
        self.name = name
        self.forcastArray = forcast
        self.lastUpdate = Date.init()
    }
    
    required init(coder decoder: NSCoder) {
        self.name = decoder.decodeObject(forKey: "name") as! String
        self.forcastArray = decoder.decodeObject(forKey: "forcastArray") as! [WeatherModel]
        self.lastUpdate = decoder.decodeObject(forKey: "lastUpdate") as! Date
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.forcastArray, forKey: "forcastArray")
        aCoder.encode(self.lastUpdate, forKey: "lastUpdate")
    }

}
